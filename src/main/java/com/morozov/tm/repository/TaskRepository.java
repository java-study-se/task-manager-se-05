package com.morozov.tm.repository;

import com.morozov.tm.api.Repository;
import com.morozov.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements Repository<Task> {
    private Map<String, Task> taskMap = new HashMap<>();

    @Override
    public List<Task> findAll() {
        List<Task> taskList = new ArrayList<>();
        taskList.addAll(taskMap.values());
        return taskList;
    }

    @Override
    public Task findOne(String id) {
        return taskMap.get(id);
    }

    @Override
    public void persist(Task writeEntity) {
        taskMap.put(writeEntity.getId(), writeEntity);
    }

    @Override
    public void merge(String id, Task updateEntity) {
        taskMap.put(id, updateEntity);
    }

    @Override
    public void remove(String id) {
        taskMap.remove(id);
    }

    @Override
    public void removeAll() {
        taskMap.clear();
    }

    public List<Task> getAllTaskByProgectId(String projectId) {
        List<Task> resultList = new ArrayList<>();
        for (Task task : taskMap.values()
        ) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    public void deleteAllTaskByProjectId(String projectId) {
        List<Task> taskToDelete = getAllTaskByProgectId(projectId);
        for (Task task : taskToDelete
        ) {
            taskMap.remove(task.getId());
        }
    }

}
