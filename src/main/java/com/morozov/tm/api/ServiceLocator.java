package com.morozov.tm.api;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.service.ProjectService;
import com.morozov.tm.service.TaskService;

import java.util.List;

public interface ServiceLocator {
    List<AbstractCommand> getCommandList();

    ProjectService getProjectService();

    TaskService getTaskService();
}
