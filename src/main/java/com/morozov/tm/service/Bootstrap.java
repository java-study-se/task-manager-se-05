package com.morozov.tm.service;

import com.morozov.tm.command.*;
import com.morozov.tm.exception.CommandCorruptException;
import com.morozov.tm.api.ServiceLocator;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.util.ConsoleHelper;
import java.util.*;

public class Bootstrap implements ServiceLocator {

    private ProjectService projectService = new ProjectService(new ProjectRepository());
    private TaskService taskService = new TaskService(new TaskRepository());
    private Map<String, AbstractCommand> commands = new TreeMap<>();

    public void init() {
        fillCommandMap();
        ConsoleHelper.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        String console = "";
        while (!"exit".equals(console)) {
            console = ConsoleHelper.readString();
            AbstractCommand command = commands.get(console);
            if (command != null) {
                command.execute();
            } else {
                if (!"exit".equals(console))
                    ConsoleHelper.writeString("Команда не распознана, введите повторно. Для вызова справки введите \"help\"");
            }
        }
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    public void registry(AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void fillCommandMap() {
        AbstractCommand[] abstractCommands = {new HelpCommand(), new ProjectCreateCommand(), new ProjectClearCommand(),
                new ProjectListCommand(), new ProjectRemoveCommand(), new ProjectUpdateCommand(), new TaskClearCommand(),
                new TaskCreateCommand(), new TaskListByProjectIdCommand(), new TaskListCommand(), new TaskRemoveCommand(),
                new TaskUpdateCommand()};
        for (AbstractCommand command : abstractCommands
        ) {
            try {
                registry(command);
            } catch (CommandCorruptException e) {
                e.printStackTrace();
            }
        }
    }

}

