package com.morozov.tm.command;

import com.morozov.tm.api.ServiceLocator;


public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();
}
