package com.morozov.tm.command;

import com.morozov.tm.service.Bootstrap;
import com.morozov.tm.util.ConsoleHelper;

public class HelpCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all command";
    }

    @Override
    public void execute() {
        for (AbstractCommand command: serviceLocator.getCommandList()
             ) {
            ConsoleHelper.writeString(String.format("%s: %s", command.getName(), command.getDescription()));
        }
    }
}
