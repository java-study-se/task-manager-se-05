package com.morozov.tm.command;

import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "Update selected project";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Введите ID проекта для изменения");
        String updateProjectId = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое имя проекта");
        String updateProjectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новое описание проекта");
        String updateProjectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        String startUpdateDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        String endUpdateDate = ConsoleHelper.readString();
        try {
            serviceLocator.getProjectService().updateProject(updateProjectId, updateProjectName, updateProjectDescription, startUpdateDate, endUpdateDate);
            ConsoleHelper.writeString("Проект изменен");
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
