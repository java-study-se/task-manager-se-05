package com.morozov.tm.command;

import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Введите порядковый номер задачи для удаления");
        String idDeletedTask = ConsoleHelper.readString();
        try {
            if (serviceLocator.getTaskService().deleteTask(idDeletedTask)) {
                ConsoleHelper.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
            }
            ConsoleHelper.writeString("Задачи с данным ID не существует");
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        }
    }
}
