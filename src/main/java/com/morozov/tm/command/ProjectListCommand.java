package com.morozov.tm.command;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.util.ConsoleHelper;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        try {
            List<Project> projectList = serviceLocator.getProjectService().getAllProject();
            ConsoleHelper.writeString("Список проектов:");
            for (int i = 1; i <= projectList.size(); i++) {
                ConsoleHelper.writeString(String.format("%d: %s", i, projectList.get(i).toString()));
            }
        } catch (RepositoryEmptyException e) {
            ConsoleHelper.writeString("Список проектов пуст");
        }
    }
}
