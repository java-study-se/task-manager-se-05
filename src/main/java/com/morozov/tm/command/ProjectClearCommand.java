package com.morozov.tm.command;

import com.morozov.tm.util.ConsoleHelper;

public class ProjectClearCommand extends AbstractCommand{
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().clearTaskList();
        ConsoleHelper.writeString("Список задач очищен");
        serviceLocator.getProjectService().clearProjectList();
        ConsoleHelper.writeString("Список проектов очищен");
    }
}
