package com.morozov.tm.command;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Введите имя нового проекта");
        String projectName = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите описание нового проекта");
        String projectDescription = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату начала нового проекта в формате DD.MM.YYYY");
        String startProjectDate = ConsoleHelper.readString();
        ConsoleHelper.writeString("Введите дату окончания нового проекта в формате DD.MM.YYYY");
        String endProjectDate = ConsoleHelper.readString();
        try {
            Project addedProject = serviceLocator.getProjectService().addProject(projectName, projectDescription, startProjectDate, endProjectDate);
            ConsoleHelper.writeString("Добавлен проект: " + addedProject.toString());
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("Введенные поля не могут быть пустими");
        } catch (ParseException e) {
            ConsoleHelper.writeString("Неверный формат даты. Введите дату в формате DD.MM.YYYY");
        }
    }
}
