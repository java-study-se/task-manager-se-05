package com.morozov.tm.command;

import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelper;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() {
        ConsoleHelper.writeString("Введите ID проекта для удаления");
        String idDeletedProject = ConsoleHelper.readString();
        ConsoleHelper.writeString("Удаление задач с ID проекта: " + idDeletedProject);
        serviceLocator.getTaskService().deleteAllTaskByProjectId(idDeletedProject);
        try {
            if (serviceLocator.getProjectService().deleteProject(idDeletedProject)) {
                ConsoleHelper.writeString("Проект с ID: " + idDeletedProject + " удален");
            } else {
                ConsoleHelper.writeString("Проекта с данным ID не существует");
            }
        } catch (StringEmptyException e) {
            ConsoleHelper.writeString("ID не может быт пустым");
        }
    }
}
